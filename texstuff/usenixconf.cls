%% -*- mode: LaTeX -*-
%%
%% Copyright (c) 2000, 2006, 2007 The University of Utah and the Computer
%% Systems Laboratory at the University of Utah (CSL).
%%

%% usenixconf.cls --- USENIX conference version of the `article' class
%%
%% `usenixconf' is a LaTeX document class for producing USENIX conference
%% papers.  This class is a straightforward modification of the standard LaTeX
%% `article' class and incorporates the `usenix.sty' style file that is
%% distributed by USENIX itself.  The principal features of `usenixconf.cls'
%% over `usenix.sty' alone are:
%%
%%   + an `\affiliation' command for defining the authors' affiliations, which
%%     will appear below the authors' names in the title information;
%%   + a `\toappear' command for inserting a publication notice above the
%%     paper title (enabled by the `preprint' and `techreport' class options);
%%   + an `\ifpreprint' conditional, set when the `preprint' class option is
%%     specified;
%%   + an `\iftechreport' conditional, set when the `techreport' class option
%%     is specified;
%%
%%   + the ``mandatory options'' are automatically given to the underlying
%%     LaTeX `article' class, e.g., `twocolumn';
%%   + page numbers in preprint and techreport copies; and
%%   + better vertical space around section and subsection headings.
%%
%% The idea for `\affiliation' and `\toappear' were taken from the `acmconf'
%% document class by Ken Traub, Olin Shivers, Peter Lee, Simon Peyton Jones,
%% and David A. Berson.

\NeedsTeXFormat{LaTeX2e}
\def\filename{usenixconf.cls}
\def\filedate{2007/01/09}
\def\fileversion{0.5}
\ProvidesClass{usenixconf}[\filedate\space\fileversion\space
  USENIX conference paper document class]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Declare and process the class options.  In addition to most of the standard
%% `article' class options, this document class provides three additional
%% options:
%%
%%   `preprint',    for preparing preprint copies of a paper.
%%   `techreport',  for preparing technical report versions of a paper.
%%
%%   `pagenumbers', for outputting page numbers.  This is something of a hack
%%                  for submission copies of papers, which should generally
%%                  have numbered pages.  Beyond enabling page numbers, this
%%                  option preserves all page-style commands.
%%
%% This class disables the standard `titlepage' and `onecolumn' options to the
%% `article' class.

\newif\if@pagestyle
\@pagestylefalse
\newif\if@toappear
\@toappearfalse

\newif\ifpreprint
\preprintfalse
\DeclareOption{preprint}%
  {\preprinttrue\@pagestyletrue\@toappeartrue}
\newif\iftechreport
\techreportfalse
\DeclareOption{techreport}%
  {\techreporttrue\@pagestyletrue\@toappeartrue}
\DeclareOption{pagenumbers}%
  {\@pagestyletrue}

\DeclareOption{onecolumn}%
  {\ClassError{usenixconf}%
     {`onecolumn' format is not allowed for USENIX conference papers}}
\DeclareOption{titlepage}%
  {\ClassWarningNoLine{usenixconf}%
     {`titlepage' format is not supported for USENIX conference papers}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}

\ProcessOptions


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Load the standard `article' class.

\LoadClass[10pt,letterpaper,oneside,notitlepage,twocolumn]{article}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Below is the USENIX-provided `usenix.sty', with a few lines commented out.
%% Look for `%% ENE'.
%%
%% The following summary of the USENIX Proceedings format was published at
%% <http://www.usenix.org/events/osdi2000/instrux/instructions.html>:
%%
%%   * Length: maximum 14 pages 
%%   * 2 columns: each 3.125 inches ('') wide 
%%   * Gutter between columns: .25''
%%   * Left/right margin: 1''
%%   * Top margin: 1''
%%   * Bottom margin: 1''
%%   * Font: Times Roman strongly preferred
%%   * Title: 14 pt bold
%%   * Text: 10 pt type on 12 pt leading
%%   *   Attribution:
%%   *     Author's name: 12 pt roman
%%   *     Author's affiliation: 12 pt italic
%%   * Headings: 12 pt bold
%%   * Footnotes, equations, & illustration captions: no smaller than 8 point
%%   * Illustrations: black & white only
%%
%% Notes:
%%
%%   + This class typesets the paper title in 14.4pt bold (`\Large\bfseries').
%%     Close enough!  The authors' names are set in 12pt (`\large') and the
%%     affiliations are set in 12pt italic (`\large\itshape').  Captions are
%%     set in 9pt (`\small').
%%
%%   + We don't enforce the total paper length :-) or the font.  Maybe we
%%     should enforce the font.

% TEMPLATE for Usenix papers, specifically to meet requirements of
%  TCL97 committee.
% originally a template for producing IEEE-format articles using LaTeX.
%   written by Matthew Ward, CS Department, Worcester Polytechnic Institute.
% adapted by David Beazley for his excellent SWIG paper in Proceedings,
%   Tcl 96
% turned into a smartass generic template by De Clarke, with thanks to
%   both the above pioneers
% use at your own risk.  Complaints to /dev/null.
% make it two column with no page numbering, default is 10 point

% include following in document.
%\documentclass{article}
%\usepackage{usits,epsfig,twocolumn}
%% ENE \pagestyle{empty}

%set dimensions of columns, gap between columns, and space between paragraphs
%\setlength{\textheight}{8.75in}
\setlength{\textheight}{9.0in}
\setlength{\columnsep}{0.25in}
\setlength{\textwidth}{6.5in}
\setlength{\footskip}{0.0in}
\setlength{\topmargin}{0.0in}
\setlength{\headheight}{0.0in}
\setlength{\headsep}{0.0in}
\setlength{\oddsidemargin}{0in}
%\setlength{\oddsidemargin}{-.065in}
%\setlength{\oddsidemargin}{-.17in}
%% ENE: the following style parameters are not in the modern `usenix.sty', i.e,
%% usenix.sty,v 1.2 2005/02/16
%\setlength{\parindent}{0pc}
%\setlength{\parskip}{\baselineskip}

% started out with art10.sty and modified params to conform to IEEE format
% further mods to conform to Usenix standard

%% ENE \makeatletter
%as Latex considers descenders in its calculation of interline spacing,
%to get 12 point spacing for normalsize text, must set it to 10 points
\def\@normalsize{\@setsize\normalsize{12pt}\xpt\@xpt
\abovedisplayskip 10pt plus2pt minus5pt\belowdisplayskip \abovedisplayskip
\abovedisplayshortskip \z@ plus3pt\belowdisplayshortskip 6pt plus3pt
minus3pt\let\@listi\@listI}

%need a 12 pt font size for subsection and abstract headings
\def\subsize{\@setsize\subsize{12pt}\xipt\@xipt}

%make section titles bold and 12 point, 2 blank lines before, 1 after
\def\section{\@startsection {section}{1}{\z@}{24pt plus 2pt minus 2pt}
{12pt plus 2pt minus 2pt}{\large\bf}}

%make subsection titles bold and 11 point, 1 blank line before, 1 after
\def\subsection{\@startsection {subsection}{2}{\z@}{12pt plus 2pt minus 2pt}
{12pt plus 2pt minus 2pt}{\subsize\bf}}
%% ENE \makeatother

%% ENE: from the current `usenix.sty', cleaned up somewhat.

%
% The abstract is preceded by a 12-pt bold centered heading
\renewenvironment{abstract}%
  {\begin{center}%
     {\large\bfseries \abstractname\vspace{-.5em}\vspace{\z@}}%
   \end{center}}%
  {}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Fix the most egregious problems with the above definitions.

%% ENE: the modern `usenix.sty' doesn't use blank vertical space to separate
%% pargraphs.
% \topsep=0pt
% \parskip=6pt plus2pt minus2pt
% \itemsep=\parskip
% \partopsep=0pt  % warning: affects the figures badly
\setlength{\footskip}{30pt}

%% ENE: these are essentially the same definitions as in LaTeX's standard
%% `article.cls' file, but each has been ``downsized'' about one level.
%% Compare our `\section' to the standard `\subsection'.
%%
\renewcommand{\section}{\@startsection{section}{1}{\z@}%
                                   {-3.25ex \@plus -1ex \@minus -.2ex}%
                                   {1.5ex \@plus.2ex}%
                                   {\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\subsize\bfseries}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Redefine `\maketitle' and `\@maketitle' from the standard `article' class
%% to:
%%
%%   + output a ``to appear'' notice above the paper title (if enabled);
%%   + typeset the paper title in `\Large\bfseries' (see the USENIX paper
%%     template);
%%   + include the authors' affiliations (set by `\affiliation');
%%   + omit the date;
%%   + set `\thispagestyle' to `empty', so that the first page is not numbered.

\gdef\@toappear{}
\def\toappear#1{\gdef\@toappear{#1}}

\gdef\@affiliation{}
\def\affiliation#1{\gdef\@affiliation{#1}}

\renewcommand\maketitle{\par
  \begingroup
    \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
    \def\@makefnmark{\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
    \long\def\@makefntext##1{\parindent 1em\noindent
            \hb@xt@1.8em{%
                \hss\@textsuperscript{\normalfont\@thefnmark}}##1}%
    \if@twocolumn
      \ifnum \col@number=\@ne
        \@maketitle
      \else
        \twocolumn[\@maketitle]%
      \fi
    \else
      \newpage
      \global\@topnum\z@   % Prevents figures from going at top of page.
      \@maketitle
    \fi
    \thispagestyle{empty}\@thanks
  \endgroup
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@affiliation\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\@toappear\@empty
  \global\let\toappear\relax
  \global\let\title\relax
  \global\let\author\relax
  \global\let\affiliation\relax
  \global\let\date\relax
  \global\let\and\relax
}
\def\@maketitle{%
  \newpage
  \null
  \if@toappear
    \nointerlineskip
    \vbox to 0pt{\vss\centering\@toappear\vskip-\prevdepth}%
  \fi
  \vskip 2em%
  \begin{center}%
  \let \footnote \thanks
    {\Large \bfseries \@title \par}%
%   {\LARGE \@title \par}%
    \vskip 1.5em%
    {\large
      \lineskip .5em%
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}\par}%
    \vskip 1em%
    {\large\itshape
      \lineskip .5em%
      \begin{tabular}[t]{c}%
        \@affiliation
      \end{tabular}\par}%
%   \vskip 1em%
%   {\large \@date}%
  \end{center}%
  \par
  \vskip 1.5em}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Modify the `article.cls' command that creates float captions.  Our version
%% typesets the caption in `\small'.
%%
\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip
  \sbox\@tempboxa{\small #1: #2}%
  \ifdim \wd\@tempboxa >\hsize
    {\small #1: #2\par}
  \else
    \global \@minipagefalse
    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Modify the `article.cls' environment for typesetting the bibliography.  Our
%% version omits the vertical space between the entries (by setting both
%% `\itemsep' and `\parsep' to zero).
%%
\renewenvironment{thebibliography}[1]
     {\section*{\refname}%
      \@mkboth{\MakeUppercase\refname}{\MakeUppercase\refname}%
      \list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \setlength{\itemsep}{0pt}%
            \setlength{\parsep}{0pt}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Define additional commands and turn on ``required options.''

%% Headings are ignored unless `\if@pagestyle' is true.  Normally, this is true
%% only for `preprint' or `techreport' versions of a paper.
\if@pagestyle\else
  %% `Make all `\pagestyle' commands --> `\pagestyle{empty}'.
  \let\ps@plain\ps@empty
  \let\ps@headings\ps@empty
  \let\ps@myheadings\ps@empty
  \ps@plain
\fi


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\endinput

%% End of file.
